package project.ulip;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

public class Tata {
	String tataDate;
	String tataPrice;
	String tataName = "Tata AIA Life Insurance" ;
	void selectFund(WebDriver driver, int i) {
//		Select fund = new Select(driver.findElement(By.id("ddlFund")));
//		fund.selectByValue("TGL");
		try {
			WebDriverWait wait = new WebDriverWait(driver, 30);
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id='ddlFund']"))).click();
			driver.findElement(By.xpath("//select[@id='ddlFund']/option["+"]")).click();
			} catch (Exception e) {
				System.err.println(e);
			}
	}
	void selectDay(WebDriver driver) {
		Select day = new Select(driver.findElement(By.id("ddlTimePerioddd")));
		day.selectByValue("30");
	}
	void selectMonth(WebDriver driver) {
		Select month = new Select(driver.findElement(By.id("ddlTimePeriodmm")));
		month.selectByValue("SEP");
	}
	void selectYear(WebDriver driver) {
		Select year = new Select(driver.findElement(By.id("ddlTimePeriodyy")));
		year.selectByValue("2021");
	}
	void clickSubmit(WebDriver driver) {
		driver.findElement(By.cssSelector(".submit_btn.btn_violet")).click();
	}
	void pickDate(WebDriver driver) {
		tataDate = driver.findElement(By.xpath("//div[@class='box-row']/div")).getText();
	}
	void picPrice(WebDriver driver) {
		tataPrice = driver.findElement(By.xpath("//div[@class='box-row']/div[2]")).getText();
	}
	void stopLoad(WebDriver driver) {
		//driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("return window.stop");
		//driver.findElement(By.tagName("head")).sendKeys("Keys.ESCAPE");
	}

}
