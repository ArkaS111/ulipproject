package project.ulip;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;

public class Birla {
	String birlaPrice;
	String birlaDate ;
	String birlaName ;
	String compName = "Birla";
	void pickFund(WebDriver driver , String i) {
		try {
		WebDriverWait wait = new WebDriverWait(driver, 40);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id=\"download-brochure-form\"]/div/div/div[1]"))).click();
		driver.findElement(By.xpath("//div[@class='selectize-dropdown-content']/div["+i+"]")).click();
		driver.manage().timeouts().implicitlyWait(3000, TimeUnit.SECONDS);
		} catch (Exception e) {
			System.err.println(e);
		}
	}
	void pickPrice(WebDriver driver) {
		birlaPrice = driver.findElement(By.xpath("//label[text()='Unit Price']//following-sibling::p")).getText();
		driver.manage().timeouts().implicitlyWait(1000, TimeUnit.SECONDS);
	}
	void pickDate(WebDriver driver) {
		birlaDate = driver.findElement(By.xpath("/html/body/div[3]/div[1]/div/div/div[7]/ul/li[4]/p")).getText();
		//driver.manage().timeouts().implicitlyWait(1000, TimeUnit.SECONDS);
	}
	void pickName(WebDriver driver) {
		birlaName = driver.findElement(By.xpath("//label[text()='Fund Name']//following-sibling::p")).getText();
		driver.manage().timeouts().implicitlyWait(1000, TimeUnit.SECONDS);
	}
}
