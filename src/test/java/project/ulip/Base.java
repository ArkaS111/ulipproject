package project.ulip;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class Base {
	public static void main(String[] args) {
		List<String[]> aegon = new ArrayList<String[]>();
		List<String[]> birla = new ArrayList<String[]>();
		List<String[]> tata = new ArrayList<String[]>();
		Initializer initializer = new Initializer();
		WebDriver driver = initializer.init();
		initializer.openAegonlife(driver);
		Aegonlife al = new Aegonlife();
		for (int i = 3; i <= 8; i++) {
			Integer io = new Integer(i);
			al.selectSchemeName(driver, io.toString());
			al.selectStartDate(driver);
			al.selectEndDate(driver);
			al.clickSearch(driver);
			al.pickDate(driver);
			al.pickPrice(driver);
			al.pickName(driver);
			String[] temp = {al.compName, al.aegonName, al.aegonDate, al.aegonPrice };
			aegon.add(temp);

		}
		//initializer.closeWindow(driver);
		
		initializer.openAdityabirla(driver);
		Birla b = new Birla();
		for (int i = 1; i < 10; i++) {
			Integer io = new Integer(i);
			b.pickFund(driver, io.toString());
			b.pickDate(driver);
			b.pickPrice(driver);
			b.pickName(driver);
			String[] temp = { b.compName,b.birlaName, b.birlaDate, b.birlaPrice };
			birla.add(temp);
		}
		//initializer.closeWindow(driver);
		
//		initializer.openTataaia(driver);
//		Tata tt = new Tata();
//		for (int i = 2; i < 5; i++) {
//			tt.stopLoad(driver);
//			tt.selectFund(driver, i);
//			tt.selectDay(driver);
//			tt.selectMonth(driver);
//			tt.selectYear(driver);
//			tt.clickSubmit(driver);
//			tt.pickDate(driver);
//			tt.picPrice(driver);
//			String[] temp = { tt.tataName, tt.tataDate, tt.tataPrice };
//			tata.add(temp);
//			tt.stopLoad(driver);
//		}

		new CSVcreator().writeDataLineByLine(aegon, birla/*, tata*/);

		initializer.closeWindow(driver);

	}

}
